# Тестовое задание 

Перед запуском необходимо
  - Запустить сервис elasticsearch
  - Установить зависимости `pip install -r requirements.txt`
  - Сделать миграции в базу данных `python manage.py migrate`
  - Загрузить в базу стандартные данные `python manage.py loaddata db-startdata.json`
  - Создать индекс `python manage.py search_index --rebuild -f`

Запросы к api хранятся в файле `api_requests.py`