from rest_framework import serializers

from .models import City, User, Announcement


class RegisterUserSerializer(serializers.Serializer):
    login = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=20, write_only=True)
    name = serializers.CharField(max_length=20, default=None)
    city = serializers.SlugRelatedField(queryset=City.objects.all(), slug_field='name')

    api_key = serializers.CharField(read_only=True, )

    def validate(self, data):
        if data['name'] is None:
            data['name'] = data['login']

        return data

    def create(self, validated_data=None):
        if validated_data is None:
            validated_data = self.validated_data
        return User.objects.create_user(**validated_data)


class LoginUserSerializer(serializers.Serializer):
    login = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=20, write_only=True)


class CitySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=30)

    def create(self, validated_data=None):
        if validated_data is None:
            validated_data = self.validated_data
        return City.objects.create(**validated_data)



class AnnouncementSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=50)
    user = serializers.CharField(source='user.name', read_only=True)
    city = serializers.SlugRelatedField(queryset=City.objects.all(), slug_field='name')
    body = serializers.CharField(max_length=1000, default='')
    price = serializers.IntegerField(default=None)

    def create(self, **kwargs):
        data = {**self.validated_data, **kwargs}
        return Announcement.objects.create(**data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.city = validated_data.get('city', instance.city)
        instance.body = validated_data.get('body', instance.body)
        instance.price = validated_data.get('price', instance.price)
        instance.save()
        return instance

