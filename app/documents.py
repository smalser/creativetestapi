from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from .models import User, Announcement, City


@registry.register_document
class AnnouncementDocument(Document):
    user = fields.TextField('user.name')
    city = fields.TextField('city.name')

    class Index:
        name = 'announcements'
        settings = {
            'number_of_shards': 1,
            'number_of_replicas': 0,
        }

    class Django:
        model = Announcement
        fields = [
            'id',
            'name',
            'body',
            'price',
        ]
        related_models = [User, City]

    def get_instances_from_related(self, related_instance):
        return None
