from django.contrib import admin

from app.models import User, City, Announcement


class UserAdmin(admin.ModelAdmin):
    fields = ('id', 'login', 'name', 'city', 'is_staff')
    list_display = fields
    readonly_fields = fields


class CityAdmin(admin.ModelAdmin):
    pass


class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('id', 'city', 'name', 'body', 'price')


# Добавим в админку модели
admin.site.register(User, UserAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Announcement, AnnouncementAdmin)
