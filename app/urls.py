from django.urls import re_path, include

from .views import RegisterUserView, LoginUserView, CitiesView, AnnouncementView

app_name = 'app'


# Адреса на пользователей
users = [
    re_path(r'^register/$', RegisterUserView.as_view(), name='register_user'),
    re_path(r'^login/$', LoginUserView.as_view(), name='login_user'),
]

# На объявления
announcements = [
    re_path(r'^$', AnnouncementView.as_view({
        'get': 'list', 'post': 'create'
    })),
    re_path(r'^(?P<announcement_id>[\w\-]+)/$', AnnouncementView.as_view({
        'get': 'retrieve', 'post': 'update', 'delete': 'destroy',
    })),
]

# На города
cities = [
    re_path(r'^$', CitiesView.as_view({'get': 'list', 'post': 'create'})),
    re_path(r'^(?P<id>[\w\-]+)/$', CitiesView.as_view({'get': 'retrieve'})),
]


# Все адреса
urlpatterns = [
    re_path(r'^users/', include(users)),
    re_path(r'^announcements/', include(announcements), name='announcements'),
    re_path(r'^cities/', include(cities), name='cities'),
]