from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager

from . import City


# Необходимо чтобы работало JWT и авторизация
# Поэтому перезапишем класс менеджера
class MyUserManager(UserManager):
    use_in_migrations = True

    def _create_user(self, login, name, password, city, **extra_fields):
        if not login:
            raise ValueError('The given username must be set')
        # Если город как сущность, берем его ид
        if isinstance(city, City):
            city = city.id

        user = self.model(login=login, name=name, city_id=city, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, login, name, city, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        return self._create_user(login, name, password, city, **extra_fields)

    def create_superuser(self, login, name, city, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        return self._create_user(login, name, password, city, **extra_fields)


class User(AbstractUser):
    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['name', 'city']

    login = models.CharField('Логин', max_length=20, null=False, unique=True, db_index=True)
    name = models.CharField('Имя пользователя', max_length=20, null=False)
    city = models.ForeignKey(City, on_delete=models.PROTECT, null=True, blank=True)

    # ПОменять местами стаф и суперюзер
    @property
    def is_superuser(self):
        return self.is_staff

    @property
    def username(self):
        return self.name

    objects = MyUserManager()

    def __str__(self):
        return f'([{self.id}] Пользователь "{self.name}" из "{self.city})"'

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'
