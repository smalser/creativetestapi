from django.db import models


class City(models.Model):
    name = models.CharField("Название города", max_length=30, null=False, unique=True, db_index=True)

    def __str__(self):
        return f'([{self.id}] Город "{self.name}")'

    class Meta:
        verbose_name = 'город'
        verbose_name_plural = 'города'
