from django.db import models

from . import User, City


class Announcement(models.Model):
    name = models.CharField('Заголовок объявления', max_length=50, null=False)
    user = models.ForeignKey(User, null=False, on_delete=models.PROTECT)
    city = models.ForeignKey(City, null=False, on_delete=models.PROTECT)
    body = models.CharField('Текст объявления', max_length=1000, default='')
    price = models.IntegerField('Цена', null=True, default=None)

    def __str__(self):
        return f'([{self.id}] Объявление "{self.body}")'

    class Meta:
        verbose_name = 'объявление'
        verbose_name_plural = 'объявления'
