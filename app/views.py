import logging

from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.pagination import LimitOffsetPagination
from rest_framework_jwt.settings import api_settings
from elasticsearch_dsl.query import Q

from app.models import User, City, Announcement
from app.documents import AnnouncementDocument
from app.serializers import (
    RegisterUserSerializer, LoginUserSerializer,
    CitySerializer, AnnouncementSerializer
)

logger = logging.getLogger(__name__)
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


# По хорошему не стоит отдавать всю базу по запросу
# Поэтому будет деление на страницы
class MyPaginator(LimitOffsetPagination):
    default_limit = 10
    max_limit = 50
    limit_query_param = 'count'
    limit_query_description = 'Количество результатов на страницу.'
    offset_query_param = 'offset'
    offset_query_description = 'Смещение, начиная с которого нужно вернуть результаты.'


# Регистрация пользователя
class RegisterUserView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request: Request, *args, **kwargs):
        serializer = RegisterUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create()
        logger.info(f'Регистрация нового пользователя {serializer.instance}')
        return Response(serializer.data, status=status.HTTP_201_CREATED)


# Авторизация пользователя
class LoginUserView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request: Request, *args, **kwargs):
        serializer = LoginUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Пробуем залогиниться
        user = authenticate(**serializer.validated_data)
        if user is None:
            logger.info(f'Не удалось авторизоваться пользователю {serializer.validated_data["login"]}')
            return Response(serializer.data, status=status.HTTP_404_NOT_FOUND)

        # Выдаем токен
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        logger.info(f'Успешная авторизация пользователя {user}')

        return Response({'token': token}, status=status.HTTP_200_OK)


class AnnouncementView(ModelViewSet):
    pagination_class = MyPaginator

    def create(self, request: Request, **kwargs):
        serializer = AnnouncementSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = serializer.create(user=request.user)
        logger.info(f'Создано новое объявление: {serializer.instance}')
        return Response(AnnouncementSerializer(obj).data, status=status.HTTP_201_CREATED)

    def retrieve(self, request: Request, announcement_id: int = None, **kwargs):
        ann = get_object_or_404(Announcement.objects.select_related('city', 'user'), id=announcement_id)
        serializer = AnnouncementSerializer(ann)
        return Response(serializer.data)

    def update(self, request: Request, announcement_id: int = None, **kwargs):
        # Пользователь не должен знать что страница существует если она не для него
        # Поэтому он получает 404 а не 401
        ann = get_object_or_404(Announcement.objects.select_related('city', 'user'),
                                user=request.user, id=announcement_id)
        serializer = AnnouncementSerializer(ann, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        logger.info(f'Обновлено объявление: {serializer.instance}')
        return Response(serializer.data)

    def destroy(self, request: Request, announcement_id: int = None, **kwargs):
        # Удалить может также и админ
        if request.user.is_staff:
            ann = get_object_or_404(Announcement, id=announcement_id)
        else:
            ann = get_object_or_404(Announcement, user=request.user, id=announcement_id)
        ann.delete()
        logger.info(f'Объявдение {ann} удалено пользователем {request.user}')
        return Response(status=status.HTTP_204_NO_CONTENT)

    def list(self, request: Request, **kwargs):
        logger.info(f'Выполняется поиск с параметрами: {request.query_params}')
        query = AnnouncementDocument.search().query()
        # Фильтрации
        for filter_field in ['city', 'user']:
            field = request.query_params.get(filter_field, None)
            if field is not None:
                query = query.filter('match', **{filter_field: field})

        # Поиск и fuzziness
        search = request.query_params.get('search', None)
        if search is not None:
            q_body = {'query': search, 'fuzziness': 'AUTO'}
            query = query.query(Q('match', name=q_body) | Q('match', body=q_body))

        # Сортировка
        sort = request.query_params.get('sort', None)
        if sort == '1':
            query = query.sort('id')
        elif sort == '-1':
            query = query.sort('-id')

        # Запрос с пагинатором
        queryset = self.paginate_queryset(query.to_queryset().select_related('city', 'user'))
        serializer = AnnouncementSerializer(queryset, many=True)
        return Response({'announcements': serializer.data})

    # Метод переопределения уровня доступа
    def get_permissions(self):
        if self.action in ('list', 'retrieve'):
            permission_classes = (AllowAny, )
        else:
            permission_classes = (IsAuthenticated, )
        return [permission() for permission in permission_classes]


# Тут не будем усложнять, пускай все сделают за нас
class CitiesView(ModelViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()
    lookup_field = 'id'

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = (IsAdminUser, )
        else:
            permission_classes = (AllowAny, )
        return [permission() for permission in permission_classes]
