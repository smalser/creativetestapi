"""
Листик с запросами
"""
from requests import Session, Response
from uuid import uuid4
from typing import List, Dict, Collection


url = 'http://127.0.0.1:8000/api/'
anon = Session()



############################
def login(login: str = 'admin', password: str = 'admin') -> Session:
    """ Авторизация пользователя

    Args:
        login (str): Логин. Defaults to 'admin'.
        password (str): Пароль. Defaults to 'admin'.

    Returns:
        Session: Сессия с выполненным входом
    """
    data = {
        'login': login,
        'password': password,
    }
    s = Session()
    r = s.post(url + 'users/login/', json=data)
    print('Login:', r.status_code, r.text)
    s.headers['Authorization'] = f'JWT {r.json()["token"]}'
    return s


def create_user(login: str = 'user2', name: str = 'user2', password: str = "user",
                 city: str = 'Saint-Petersburg') -> Response:
    """СОздание пользователя

    Args:
        login (str): Логин пользователя. Defaults to 'user2'.
        name (str): Имя пользователя (Если не указано, использвется логин). Defaults to 'user2'.
        password (str): Пароль, устанавливаемый пользователю. Defaults to "user".
        city (str): Город. Defaults to 'Saint-Petersburg'.

    Returns:
        Response: Объект Response
    """
    data = {
        'login': login,
        'name': name if name is not None else login,
        'password': password,
        'city': city,
    }
    
    r = anon.post(url + 'users/register/', json=data)
    print(r.status_code, r.text)
    return r


def get_announcements(s: Session, id: int = None) -> List[dict]:
    """ Получить список объявлений (или одно объявление если указан конкретный id)

    Args:
        s (Session): Сессия пользователя
        id (int): Ид объявления. Defaults to None.

    Returns:
        List[dict]: Список возвращенных объявлений
    """
    if id is not None:
        r = s.get(url+f'announcements/{id}/')
    else:
        r = s.get(url+f'announcements/')
    print(r.status_code, r.text)
    return r.json()

def search_announcements(s: Session, user: str = None, city: str = None,
                          search: str = None, sort: int = 1,
                          count: int = 10, offset: int = 0) -> List[dict]:
    """ Поиск объявлений по параметрам

    Args:
        s (Session): Сессия пользователя
        user (str): Фильтрация по имени пользователя. Defaults to None.
        city (str): Фильтрация по городу. Defaults to None.
        search (str): Строка поиска. Defaults to None.
        sort (int): Сортировка: 1 - прямая, -1 - обратная. Defaults to 1.
        count (int): Количество возвращаемых результатов. Defaults to None.
        offset (int): Смещение от начала списка. Defaults to None.

    Returns:
        List[dict]: Список возвращенныех объявлений
    """
    params = {
        'sort': sort,
        'count': count,
        'offset': offset,
    }
    if user is not None: params['user'] = user
    if city is not None: params['city'] = city
    if search is not None: params['search'] = search
        
    r = s.get(url + 'announcements/', params=params)
    print(r.status_code, r.text)
    return r.json()

def create_announcement(s: Session, name: str = 'Объявление', body: str = 'С некоторым описанием',
                         city: str = 'Moscow', price: int = 100) -> Response:
    """ Создание объявления

    Args:
        s (Session): Сессия пользователя
        name (str): Название объявления. Defaults to 'Объявление'.
        body (str): Текст объявления. Defaults to 'С некоторым описанием'.
        city (str): Город. Defaults to 'Moscow'.
        price (int): Стоимость. Defaults to 100.

    Returns:
        Response: Объект Response
    """
    data = {
        'name': name,
        'body': body,
        'city': city,
        'price': price,
    }

    r = s.post(url + 'announcements/', json=data)
    print(r.status_code, r.json())
    return r

def update_announcement(s: Session, id: int, name: str = None, body: str = None,
                         city: str = None, price: int = None) -> Response:
    """ Обновление объявления

    Args:
        s (Session): Сессия пользователя
        id (int): Ид объявления
        name (str): Новое имя. Defaults to None.
        body (str): Новый текст. Defaults to None.
        city (str): Новый город. Defaults to None.
        price (int): Новая цена. Defaults to None.

    Returns:
        Response: объект Response
    """
    data = {}
    if name is not None: data['name'] = name
    if body is not None: data['body'] = body
    if city is not None: data['city'] = city
    if price is not None: data['price'] = price

    r = s.post(url + f'announcements/{id}/', json=data)
    print(r.status_code, r.text)
    return r

def delete_announcement(s: Session, id: int) -> Response:
    """ Удаление объявления 

    Args:
        s (Session): Сессия пользователя
        id (int): Ид объявления

    Returns:
        Response: Объект Response
    """
    r = s.delete(url + f'announcements/{id}/')
    print(r.status_code, r.text)
    return r


####################################################
def create_and_login():
    """
    Создание пользователя и вход
    """
    global admin, user1, user2
    admin = login()
    user1 = login('user', 'user')
    try:
        # Если user2 уже создан
        user2 = login('user2', 'user')
    except:
        # Если еще нет
        create_user()
        user2 = login('user2', 'user')


def check_announcements():
    # Проверка чтения
    state1 = get_announcements(anon)
    # Проверка доступа
    assert get_announcements(anon) == get_announcements(admin) == get_announcements(user2) == get_announcements(user1)
    
    # Создание
    a1 = create_announcement(user1)
    # Снова доступ
    assert get_announcements(anon) == get_announcements(admin) == get_announcements(user2)== get_announcements(user1)
    
    # Проверка на совпадение
    a2 = create_announcement(user2).json()
    a2_ = get_announcements(anon, id=a2['id'])
    assert a2 == a2_
    
    # Доступ
    assert update_announcement(admin, id=a2['id'], city='Kazan').status_code == update_announcement(user1, id=a2['id'], city='Kazan').status_code == 404
    assert update_announcement(anon, id=a2['id'], city='Kazan').status_code == 401
    # Изменение
    a2_changed = update_announcement(user2, id=a2['id'], city='Saint-Petersburg').json()
    a2_changed_ = get_announcements(anon, id=a2['id'])
    assert a2_changed == a2_changed_
    
    # Удаление 
    assert delete_announcement(anon, id=a1.json()['id']).status_code == 401
    assert delete_announcement(user2, id=a1.json()['id']).status_code == 404
    assert delete_announcement(user1, id=a1.json()['id']).status_code == 204
    assert delete_announcement(admin, id=a2['id']).status_code == 204
    
    state2 = get_announcements(anon)
    assert state1 == state2


# Некоторые запросы на поиск
def announcements_search():
    search_announcements(anon)
    search_announcements(anon, user='user')
    search_announcements(anon, user='user', sort=-1)
    search_announcements(anon, sort=-1, city="Moscow")
    search_announcements(anon, sort=-1, city="Kazan")
    search_announcements(anon, sort=-1, city="Kazan", search='квартира')
    search_announcements(anon, sort=-1, city="Kazan", search='телевмзоо')
    search_announcements(anon, search='умная')

if __name__ == "__main__":
    create_and_login()
    check_announcements()
    announcements_search()